package com.markhsiu.minimq.remote;

/**
 * 地址信息
 * Created by Mark Hsiu on 2017/2/8.
 */
public class Address {

    private int port = 9090 ;
    private String hostname = "127.0.0.1";

    public Address(){
    }
    
    public Address(String host,int port){
    	this.hostname = host;
    	this.port = port;
    }
    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
    
    public String toString(){
    	return hostname + ":" + port;
    }
}
