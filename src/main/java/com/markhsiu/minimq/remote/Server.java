package com.markhsiu.minimq.remote;

/**
 * 远程访问服务端接口
 * Created by Mark Hsiu on 2017/2/8.
 */
public interface Server {
     Address getAddr();
     void open();
     void close();

}
