package com.markhsiu.minimq.remote.transport.talentaio;

import com.talent.aio.common.intf.Packet;

public class MessagePacket extends Packet{

	public static final int HEADER_LENGHT = 4;//消息头的长度
	public static final String CHARSET = "utf-8";
	private byte[] body;
	
	public byte[] getBody() {
		return body;
	}
	
	public void setBody(byte[] body) {
		this.body = body;
	}
	
	
}
