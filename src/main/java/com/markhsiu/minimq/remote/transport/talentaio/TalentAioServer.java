package com.markhsiu.minimq.remote.transport.talentaio;

import java.io.IOException;

import com.markhsiu.minimq.core.exeption.MiniMQException;
import com.markhsiu.minimq.remote.AbstractServer;
import com.markhsiu.minimq.remote.Address;
import com.markhsiu.minimq.remote.transport.talentaio.handler.MessageServerAioHandler;
import com.talent.aio.server.AioServer;
import com.talent.aio.server.ServerGroupContext;
import com.talent.aio.server.intf.ServerAioHandler;
import com.talent.aio.server.intf.ServerAioListener;

public class TalentAioServer extends AbstractServer{

	private  ServerGroupContext<Object, MessagePacket, Object> serverGroupContext = null;
	private  AioServer<Object, MessagePacket, Object> aioServer = null; //可以为空
	private  ServerAioHandler<Object, MessagePacket, Object> aioHandler = null;
	private  ServerAioListener<Object, MessagePacket, Object> aioListener = null;

	public TalentAioServer(Address serverURL) {
		if (serverURL == null) {
			throw new MiniMQException(" server address is null");
		}
		addr = serverURL;
	}
	 

	public TalentAioServer() {}


	@Override
	public void open() {
		aioHandler = new MessageServerAioHandler(super.handlers);
		aioListener = null; //可以为空
		serverGroupContext = new ServerGroupContext<>(aioHandler, aioListener);
		serverGroupContext.setEncodeCareWithChannelContext(false);
		serverGroupContext.setReadBufferSize(2048);
		aioServer = new AioServer<>(serverGroupContext);
		try {
			aioServer.start(addr.getHostname(), addr.getPort());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void close() {
		aioServer.stop();
	}

}
