package com.markhsiu.minimq.remote.transport.netty;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.markhsiu.minimq.channel.ChannelProcessor;
import com.markhsiu.minimq.message.Message;

import io.netty.channel.ChannelHandlerContext;

/**
 *  netty channel 适配器
 * @author Mark Hsiu
 *
 */
public class NettyChannelAdapterProcessor implements ChannelProcessor<Message>{
	private static Logger logger = LoggerFactory.getLogger(NettyChannelAdapterProcessor.class);
	private static boolean isDebugEnabled = logger.isDebugEnabled();
	private ChannelHandlerContext channel;
	
	@SuppressWarnings("unused")
	private  NettyChannelAdapterProcessor() {}
	
	public NettyChannelAdapterProcessor(ChannelHandlerContext ctx){
		this.channel = ctx;
	}
	
	@Override
	public void write(Message message) {
		channel.write(message);
	}

	@Override
	public void flush() {
		channel.flush();
	}

	@Override
	public void writeAndFlush(Message message) {
		if(isDebugEnabled){
			logger.debug("channelWrite ... {}",message);
		}	
		channel.writeAndFlush(message);
	}

}
