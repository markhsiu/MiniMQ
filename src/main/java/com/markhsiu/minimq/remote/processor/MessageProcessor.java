package com.markhsiu.minimq.remote.processor;

import com.markhsiu.minimq.channel.ChannelProcessor;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;

/**
 *  服务端接受消息处理接口
 * @author Mark Hsiu
 *
 */
public interface MessageProcessor {
	
	 MessageSourceEnum DEFAULT = MessageSourceEnum.UNKNOWN;
	 void handler(final Message request,ChannelProcessor<Message> channel);
	
}
