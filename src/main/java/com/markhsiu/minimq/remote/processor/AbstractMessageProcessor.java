package com.markhsiu.minimq.remote.processor;

import com.markhsiu.minimq.channel.ChannelProcessor;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;

/**
 * 消息处理抽象类
 * @author Mark Hsiu
 *
 */
public abstract class AbstractMessageProcessor implements MessageProcessor{

	/**
	 *  验证消息
	 * @param message
	 * @return
	 */
	protected boolean checkMassage(Message message) {
		if(message.getCmd() == null){
			return false;
		}
		
		if(message.getSource() == MessageSourceEnum.CUSTOMER){
			if(message.getConsumerID() == null 
					|| message.getConsumerID().trim().length() == 0){
				return false;
			}
			if(message.getCmd() == MessageCmdEnum.SUB 
					|| message.getCmd() == MessageCmdEnum.UNSUB){
				if(message.getTopic() == null || 
						message.getTopic().trim().length() == 0){
					return false;
				}
			}
		}
		
		return true;
	}
	
	public abstract void handler(final Message request,ChannelProcessor<Message> channel);
}
