package com.markhsiu.minimq.remote.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.markhsiu.minimq.channel.ChannelProcessor;
import com.markhsiu.minimq.message.Message;

/**
 *  默认处理消息器
 * @author Administrator
 *
 */
public  class DefaultMessageProcessor implements MessageProcessor{

	private static Logger logger = LoggerFactory.getLogger(DefaultMessageProcessor.class);
	private static boolean isErrorEnabled = logger.isErrorEnabled();
	
	@Override
	public void handler(Message request, ChannelProcessor<Message> channel) {
		if(isErrorEnabled){
			logger.error("未知请求类型");
			logger.error(request.toString());
		}
		
	}

	 
}
