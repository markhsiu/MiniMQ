package com.markhsiu.minimq.remote.processor;

import com.markhsiu.minimq.broker.store.StoreManager;
import com.markhsiu.minimq.channel.ChannelProcessor;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;
import com.markhsiu.minimq.remote.processor.AbstractMessageProcessor;

/**
 *  生产者连接消息处理器
 * @author Mark Hsiu
 *
 */
public class ProducerMessageProcessor extends AbstractMessageProcessor{
  
	@Override
	public void handler(final Message request,ChannelProcessor<Message> channel) { 
		if(!checkMassage(request)){
			channel.writeAndFlush(Message.newError(request));
			return;
		}
		
		
		Message response = Message.newSuccess(request) ;
		response.setMessageID(request.getMessageID());
		response.setSource(MessageSourceEnum.BROKER);
		response.setTarget(MessageSourceEnum.PRODUCER);
		switch (request.getCmd()) {	
			case NEW:	
				//优化
				StoreManager.addMessage(request);
				break;
			default:
				response.setCmd(MessageCmdEnum.FAIL);
				break;
		}
	 
		channel.writeAndFlush(response);
	}
}
