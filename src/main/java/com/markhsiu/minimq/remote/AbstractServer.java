package com.markhsiu.minimq.remote;

import java.util.HashMap;
import java.util.Map;

import com.markhsiu.minimq.message.constant.MessageSourceEnum;
import com.markhsiu.minimq.remote.processor.CustomerMessageProcessor;
import com.markhsiu.minimq.remote.processor.DefaultMessageProcessor;
import com.markhsiu.minimq.remote.processor.MessageProcessor;
import com.markhsiu.minimq.remote.processor.ProducerMessageProcessor;

public abstract class AbstractServer implements Server{

	 protected final Map<MessageSourceEnum, MessageProcessor> handlers = new HashMap<>();
	 
	 protected Address addr;
	 protected  AbstractServer(){
		 addr = new Address();
		 handlers.put(MessageSourceEnum.CUSTOMER, new CustomerMessageProcessor());
	     handlers.put(MessageSourceEnum.PRODUCER, new ProducerMessageProcessor());
	     handlers.put(MessageSourceEnum.UNKNOWN, new DefaultMessageProcessor());
	 }
	 
	 public  Address getAddr(){
		 return addr;
	 }
	 public abstract void open();
	 public abstract void close();
}
