package com.markhsiu.minimq.broker.store;

import com.markhsiu.minimq.message.Message;

public interface Store {

	public void addMessage(final Message e);
	public Message pollMessage(String topic);
}
