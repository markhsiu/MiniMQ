package com.markhsiu.minimq.broker.store;

import com.markhsiu.minimq.core.constant.ConstantUtil;
import com.markhsiu.minimq.message.Message;

public class StoreManager {

	private static final Store store = new DiskStore();
	 
	// ----------- 消息存储管理
	public static void addMessage(final Message e) {
		ConstantUtil.threadPools(new Runnable() {

			@Override
			public void run() {
				store.addMessage(e);
			}
		});
		
	}

	public static Message pollMessage(String topic) {		
		return store.pollMessage(topic);
	}

 
}
