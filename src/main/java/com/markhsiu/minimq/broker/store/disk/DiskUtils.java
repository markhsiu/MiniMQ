package com.markhsiu.minimq.broker.store.disk;

import java.io.File;
import java.text.MessageFormat;

/**
 *  磁盘存储相关工具和常量
 * @author Mark Hsiu
 *
 */
public class DiskUtils {

	public static final String ROOT_PATH = "E:\\test\\mq\\";
	public static final String INDEX_FILE_SUFFIX = ".index";//topic.index
	public static final String DATA_FILE_SUFFIX = ".block";//topic_1.block
	public static final String SEP = File.separator;
	
	public static boolean isIndexFile(String fileName) {
	      return fileName.endsWith(DiskUtils.INDEX_FILE_SUFFIX);
	}
	
	public static String truncateTopicName(String fileName){
		return fileName.substring(0, fileName.indexOf("."));
	}
	
	 
	public static String blockFileName(String topic,int num) {
		return MessageFormat.format("{0}_{1}{2}", topic, num,
				DiskUtils.DATA_FILE_SUFFIX);
	}
	
	public static void main(String[] args) {
		System.out.println(DiskUtils.blockFileName("topic", 1));
	}
}
