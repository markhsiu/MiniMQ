package com.markhsiu.minimq.broker.store;

import com.markhsiu.minimq.broker.store.disk.TopicDiskPool;
import com.markhsiu.minimq.message.Message;

public class DiskStore implements Store{

	@Override
	public void addMessage(Message e) {
		TopicDiskPool.putMsg(e);
	}

	@Override
	public Message pollMessage(String topic) {
		return TopicDiskPool.pollMsg(topic);
	}

}
