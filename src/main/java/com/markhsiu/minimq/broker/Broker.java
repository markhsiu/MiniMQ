package com.markhsiu.minimq.broker;

import com.markhsiu.minimq.core.constant.BaseConfig;
import com.markhsiu.minimq.core.exeption.MiniMQException;
import com.markhsiu.minimq.remote.Address;
import com.markhsiu.minimq.remote.Server;
import com.markhsiu.minimq.remote.transport.netty.NettyServer;

public class Broker extends BaseConfig{

	private Server server ;
	private volatile boolean initing = false;//是否初始化
	private volatile boolean running = false;//是否启动
	private String brokerID; 
	
	public void start(){
		if(!initing){
			throw new MiniMQException("服务相关参数没有初始化");
		}
		
		if(server == null){
			server = new NettyServer();
		}
	
		running = true;
		BrokerFactory.instance().setBroker(this).registerZk();
		server.open();
	}
	
	public void  init(String brokerID){
		this.brokerID = brokerID;
		initing = true;
	}
	
	public void shutdown(){
		if(!running){
			throw new MiniMQException("服务没有启动");
		}
		server.close();
	}
	
	public Broker setServer(Server server){
		this.server = server;
		return this;
	}
	
	public String getID(){
		return brokerID;
	}
	
	public Address getAddr(){
		return server.getAddr();
	}
}
