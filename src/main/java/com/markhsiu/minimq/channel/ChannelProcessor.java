package com.markhsiu.minimq.channel;

public interface ChannelProcessor<T>{
	
	  void write(T message);
	  void flush();
	  void writeAndFlush(T message);
	 
}