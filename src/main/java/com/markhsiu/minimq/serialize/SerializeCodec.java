package com.markhsiu.minimq.serialize;

import java.io.IOException;

public interface SerializeCodec<T> {

	final public static int MESSAGE_LENGTH = 4;

	byte[] serialize(final T message) throws IOException;
	
	T deserialize(byte[] data) throws IOException;
}
