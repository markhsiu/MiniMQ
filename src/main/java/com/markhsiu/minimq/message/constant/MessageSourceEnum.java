package com.markhsiu.minimq.message.constant;

/**
 * 消息来源
 * Created by Mark Hsiu on 2017/2/13.
 */
public enum MessageSourceEnum {

    CUSTOMER(1),//消费者
    BROKER(2),//服务端
    PRODUCER(3),//生产者
    UNKNOWN(4);//未知来源
	 
    private int source;

    private MessageSourceEnum(int source) {
        this.source = source;
    }

    public int getSource() {
        return source;
    }
}
