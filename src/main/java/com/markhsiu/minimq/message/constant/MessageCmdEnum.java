package com.markhsiu.minimq.message.constant;


/**
 * 消息命令常量
 * @author Mark Hsiu
 *
 */
public enum MessageCmdEnum {
    SUB,//消费者订阅主题
    UNSUB,//消费者取消订阅
    NEW,//生产者发送新消息
    ING,//消息存储或流转中
    SUCCESS,//操作成功
    FAIL,//操作失败
    ERROR,//请求参数有误
    CONNECT,//客户端初次连接
	DISCONNECT;//关闭客户端请求
}