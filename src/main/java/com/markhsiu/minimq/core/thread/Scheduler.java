package com.markhsiu.minimq.core.thread;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Scheduler {

	
	private ScheduledThreadPoolExecutor executor = null;
	public Scheduler(){
			executor = new ScheduledThreadPoolExecutor(2);

	}
	
	public void putTask(Runnable task){
        //mytask为线程，2是首次执行的延迟时间，最后一个参数为时间单位
//        stp.schedule(mytask, 2, TimeUnit.SECONDS);
        // 首次执行延迟2秒，之后的执行周期是1秒
//        stp.scheduleAtFixedRate(mytask, 2, 1,TimeUnit.SECONDS );
        //首次执行延迟100毫秒，之后从上一次任务结束到下一次任务开始时10毫秒
		executor.scheduleWithFixedDelay(task, 100, 10, TimeUnit.MILLISECONDS);
	}
	
	public void shutdown(){
		executor.shutdown();
	}
}
