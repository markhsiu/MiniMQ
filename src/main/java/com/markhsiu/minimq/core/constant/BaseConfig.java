package com.markhsiu.minimq.core.constant;

import org.apache.log4j.BasicConfigurator;

public class BaseConfig {

	public static final int NETTY = 1;
	public static final int TALENTAIO = 0 ;
	
	protected boolean single = true;	
	protected int transport = NETTY;

	
	public BaseConfig single(boolean single){
		this.single = single;
		return this;
	}
	
	public BaseConfig netty(){
		this.transport = NETTY;
		return this;
	}
	
	public BaseConfig talentaio(){
		this.transport = TALENTAIO;
		return this;
	}
	
	public BaseConfig() {
		 BasicConfigurator.configure();// 自动快速地使用缺省Log4j环境。
		// DOMConfigurator.configure ( String filename ) ：读取XML形式的配置文件。
		// PropertyConfigurator.configure("./log4j.properties"); // 读取使用Java的特性文件编写的配置文件。
	}

	
}
