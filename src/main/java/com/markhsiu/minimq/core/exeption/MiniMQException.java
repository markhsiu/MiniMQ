package com.markhsiu.minimq.core.exeption;

public class MiniMQException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MiniMQException() {
        super();
    }


    public MiniMQException(String message) {
        super(message);
    }
 
    public MiniMQException(String message, Throwable cause) {
        super(message, cause);
    }

    public MiniMQException( Throwable cause) {
        super(cause);
    }
    
}
