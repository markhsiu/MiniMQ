package com.markhsiu.minimq.client;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.markhsiu.minimq.message.Result;

public class CallBackCache {


	private static final int initialCapacity = 1000;
	
     /**
     * 发送消息缓存
     */
    private static final Map<String,Result> callBackMap = new ConcurrentHashMap<String, Result>(initialCapacity);
    
    
    //----------- 回调消息管理
    public static  void putCallBack(String messageID,Result result){
        callBackMap.put(messageID,result);
    }

    public static Result getCallBack(String messageID){
        return callBackMap.get(messageID);
    }
 
    public static void removeCallBack(String messageID){
        callBackMap.remove(messageID);
    }
 
}
