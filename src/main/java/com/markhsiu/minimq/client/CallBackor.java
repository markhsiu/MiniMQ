package com.markhsiu.minimq.client;

import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.Result;

public interface CallBackor {

	Message print(Result result);
}
