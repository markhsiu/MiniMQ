package com.markhsiu.minimq.client.producer;

import com.markhsiu.minimq.client.AbstractClientContainer;
import com.markhsiu.minimq.client.consumer.Consumer;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.Result;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;
import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 生产者客户端
 * Created by Mark Hsiu on 2017/2/8.
 */
public class Producer extends AbstractClientContainer{

	private static Logger logger = LoggerFactory.getLogger(Producer.class);
 
	public Producer(){
		serverAddress("127.0.01",9090);
	}
	
 
	/**
	 * 发送消息到broker
	 * @param message
	 * @return
	 */
	public Result send(Message message){
		message.setCmd(MessageCmdEnum.NEW);
		message.setSource(MessageSourceEnum.PRODUCER);;
        message.setTarget(MessageSourceEnum.BROKER);
        message.setTimestamp(System.currentTimeMillis());
		return getClient().send(message);
	}


	@Override
	public void init() {}


	/**
	 * 注册消费者信息
	 * @return
	 * @throws InterruptedException
	 * @throws KeeperException
	 */
	public void register() {

		logger.info("注册生产者信息：");
	}

}
