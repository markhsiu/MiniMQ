package com.markhsiu.minimq.client;

import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.Result;
import com.markhsiu.minimq.remote.Address;

/**
 * 远程访问客户端接口
 * Created by Mark Hsiu on 2017/2/8.
 */
public interface Client {

	/**
	 * 是已经连接上服务端
	 * 
	 * @return
	 */
	boolean isActive();
	
	/**
	 * 发送消息到服务端
	 * @param message 消息内容
	 * @return 响应内容
	 */
     Result send(Message message);
     /**
      *  设置响应回调函数
      * @param callBackor
      */
     void setCallBackor(CallBackor callBackor);

     /**
      * 获取服务端地址信息
      * @return
      */
     Address getServerAddr();
     /**
      * 设置服务端地址信息
      * @param address
      */
     void setServerAddr(Address address);
     
     /**
      *  配置客户端
      */
     void open();
     
     /**
      *  连接服务端
      */
     void connect();
     
     /**
      * 重新连接
      */
     void reConnect();

     /**
      *  关闭连接，并清理
      */
     void close();

}
