package com.markhsiu.minimq.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.markhsiu.minimq.core.exeption.MiniMQException;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.Result;
import com.markhsiu.minimq.remote.Address;

/**
 *  客户端
 * Created by Mark Hsiu on 2017/2/8.
 */
public abstract class AbstractClient implements Client {
	
	protected static Logger logger = LoggerFactory.getLogger(AbstractClient.class);
	protected static boolean isDebugEnabled = logger.isDebugEnabled();

	protected boolean active = false;
	protected Address addr = new Address(); 
	protected CallBackor callBackor;

	public AbstractClient(Address serverURL) {
		if (serverURL == null) {
			throw new MiniMQException(" server address is null");
		}
		
		setServerAddr(serverURL);
	}

	@Override
	public boolean isActive() {
		return active;
	}
	
	@Override
	public void setCallBackor(CallBackor callBackor) {
		this.callBackor = callBackor;
	}

	@Override
	public Address getServerAddr() {
		return addr;
	}

	@Override
	public void setServerAddr(Address address) {
		if(address != null){
			addr = address;
		}
	}

	@Override
	public  void open(){
		init();
	} 
	
	
	public abstract void init() ;

	@Override
	public abstract Result send(Message message) ;
		
	
	@Override
	public abstract void connect();
	
	@Override
	public  void reConnect(){
		if(active){
			throw new MiniMQException("已经连接。。。。");
		}
		connect();
	}

	@Override
	public abstract void close() ;

 

}
