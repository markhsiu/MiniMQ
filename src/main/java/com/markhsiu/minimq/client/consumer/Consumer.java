package com.markhsiu.minimq.client.consumer;

import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.markhsiu.minimq.client.AbstractClientContainer;
import com.markhsiu.minimq.client.CallBackor;
import com.markhsiu.minimq.client.Client;
import com.markhsiu.minimq.core.exeption.MiniMQException;

/**
 * 消费客户端
 * Created by Mark Hsiu on 2017/2/8.
 */
public class Consumer extends AbstractClientContainer {

	private static Logger logger = LoggerFactory.getLogger(Consumer.class);
	private CallBackor callBackor;
	/**
	 * 消费者唯一标识
	 */
	private String consumerID;
	  
	@SuppressWarnings("unused")
	private Consumer() {}
	
	
	public Consumer(String consumerID, CallBackor callBackor) {
		this.consumerID = consumerID;
		if(callBackor == null){
			throw new MiniMQException();
		}
		this.callBackor = callBackor;
	}
	
	 
	
	@Override
	public void init() {
		for (Client client : brokers) {
			client.setCallBackor(callBackor);
		}
	}

	/**
	 * 注册消费者信息
	 * @return
	 * @throws InterruptedException 
	 * @throws KeeperException 
	 */
	public void register() {
		
		logger.info("注册消费者信息："+consumerID);
	}
	
	/**
	 * 订阅主题topic
	 * @param topic
	 * @return
	 * @throws InterruptedException 
	 * @throws KeeperException 
	 */
	public void subscribe(String topic) throws KeeperException, InterruptedException{
		
		logger.info("消费者成功订阅主题："+topic);
	}
	
	/**
	 * 取消订阅主题topic
	 * @param topic
	 * @return
	 * @throws KeeperException 
	 * @throws InterruptedException 
	 */
	public void unsubscribe(String topic) throws InterruptedException, KeeperException{
		
		logger.info("消费者 取消订阅主题："+topic);
	}
 
	public String getConsumerID(){
		return consumerID;
	}
 
}
