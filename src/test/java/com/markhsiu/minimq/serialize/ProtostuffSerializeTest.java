package com.markhsiu.minimq.serialize;

import java.io.IOException;

import org.apache.commons.lang3.time.StopWatch;

import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;
import com.markhsiu.minimq.serialize.protostuff.ProtostuffCodec;

/**
 * Protostuff 序列化测试
 * @author Mark Hsiu
 *   size     ms
 *   100k      460
 *   1000k     1644
 *   10000k   11300
 *   100000k  109115
 */
public class ProtostuffSerializeTest {
	 
	private static String TEXT = "asdsa撒打算的撒旦撒旦撒旦MessageSerializeTestMessageSerializeTestMessageSerializeTestMessageSerializeTestMessageSerializeTestMessageSe"
			+ "rializeTestMessageSerializeTestMessageSerializeTestMessageSerializeTest";
	private static byte[] body = TEXT.getBytes();
	
	private static final int SIZE = 1000000;
	
	public static void main(String[] args) throws IOException {
		// 开始计时
		StopWatch watch = new StopWatch();
		watch.start();
		
		for (int i = 0; i < SIZE; i++) {
			Message message = Message.newInstance();
			message.setConsumerID("22222");
			message.setSource(MessageSourceEnum.CUSTOMER);
			message.setTarget(MessageSourceEnum.BROKER);
			message.setCmd(MessageCmdEnum.SUCCESS);
			message.setBody(body);
	    	ProtostuffCodec.OneInstance().serialize(message);
	     
	    	
		}
		watch.stop();

		String tip = String.format("调用总共耗时: [%s] 毫秒", watch.getTime());
		System.out.println(tip);
	}
}
