package com.markhsiu.minimq.talentaio;

import java.util.concurrent.TimeUnit;

import com.markhsiu.minimq.client.CallBackor;
import com.markhsiu.minimq.client.consumer.Consumer;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.Result;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;

/**
 * Created by Mark Hsiu on 2017/2/8.
 */
public class MutiCostomerTest {

	static String topic = "topic_muti_";
	static String consumerID = "topic_muti";
	
	public static void main(String[] args) {

		Consumer customer = new Consumer(consumerID, new CallBackor() {
			
			@Override
			public Message print(Result result) {
				System.out.println("print .... "+ result);
				Message response = Message.newInstance();
	        	response.setConsumerID(consumerID);
	        	response.setSource(MessageSourceEnum.CUSTOMER);
				response.setTarget(MessageSourceEnum.BROKER);
	        	response.setCmd(MessageCmdEnum.SUCCESS);
	        	return response;
			}
		});
		
		try {
			customer.talentaio();
			customer.start();
			customer.register();
			customer.subscribe(topic+1);
			TimeUnit.SECONDS.sleep(2);
			for (int i = 2; i <= 100; i++) {
				customer.subscribe(topic+i);
			}
		} catch(Exception e) {
			customer.end();
			e.printStackTrace();
		}

	}
}
