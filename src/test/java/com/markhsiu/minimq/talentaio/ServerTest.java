package com.markhsiu.minimq.talentaio;

import com.markhsiu.minimq.broker.Broker;
import com.markhsiu.minimq.remote.transport.talentaio.TalentAioServer;

/**
 * Created by Mark Hsiu on 2017/2/8.
 */
public class ServerTest {

    public static void main(String[] args) {
        Broker broker = new Broker().setServer(new TalentAioServer());	
        broker.init("broker1");
        broker.start();
    }
}
