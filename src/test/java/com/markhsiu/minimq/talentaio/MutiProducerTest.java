package com.markhsiu.minimq.talentaio;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.markhsiu.minimq.client.producer.Producer;
import com.markhsiu.minimq.core.constant.ConstantUtil;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;

public class MutiProducerTest {

	private static final int SIZE = 1000;
	private static String topic = "topic_muti_";
	private static ExecutorService pools = Executors.newFixedThreadPool(ConstantUtil.getWorkerThreads());
	
	public static void main(String[] args) throws InterruptedException {

		Producer producer = new Producer();
		try {
			producer.talentaio();
			producer.start();
			
			long start = System.currentTimeMillis();
			for (int i = 0; i < SIZE; i++) {
				for (int j = 1; j <= 100; j++) {
					final int index = j;
					pools.submit(new Runnable() {
						
						@Override
						public void run() {
							Message message = Message.newInstance();
							message.setTopic(topic+index);
							message.setCmd(MessageCmdEnum.NEW);
							message.setSource(MessageSourceEnum.PRODUCER);
							message.setTarget(MessageSourceEnum.BROKER);
							producer.send(message);
							
						}
					});
					
				}
			}
			
			// 启动一次顺序关闭，执行以前提交的任务，但不接受新任务。
			pools.shutdown();

			try {
				// 请求关闭、发生超时或者当前线程中断，无论哪一个首先发生之后，都将导致阻塞，直到所有任务完成执行
				// 设置最长等待10秒
				pools.awaitTermination(1000, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				//
				e.printStackTrace();
			}

			System.out.println("主线执行。");
			long end  = System.currentTimeMillis();
			System.out.println("花费 "+(end-start)+" ms");
		} finally {
			producer.end();
		}

	}
}
