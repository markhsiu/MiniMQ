package com.markhsiu.minimq.netty;

import java.util.concurrent.CountDownLatch;

import org.apache.commons.lang3.time.StopWatch;

import com.markhsiu.minimq.MutiThread;
import com.markhsiu.minimq.client.producer.Producer;

/**
 * 
 * 10k是个临界值，越大性能验证下降
 * @author Mark Hsiu
 *
 */
public class MutiThreadTest {

	// 并行度10000
	private static final int parallel = 10000; 
	
	
	public static void main(String[] args) throws Exception {
		
		CountDownLatch signal = new CountDownLatch(1);
		CountDownLatch finish = new CountDownLatch(parallel);
		Producer producer = new Producer();
		producer.start();

		// 开始计时
		StopWatch watch = new StopWatch();
		watch.start();


		for (int index = 0; index < parallel; index++) {
			MutiThread client = new MutiThread(producer,signal, finish);
			new Thread(client).start();
		}

		// 10000个并发线程瞬间发起请求操作
		signal.countDown();
		finish.await();

		watch.stop();

		String tip = String.format("调用总共耗时: [%s] 毫秒", watch.getTime());
		System.out.println(tip);

	}
}
