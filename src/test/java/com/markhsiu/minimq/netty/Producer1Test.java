package com.markhsiu.minimq.netty;

import com.markhsiu.minimq.client.producer.Producer;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.Result;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;

public class Producer1Test {

	private static final int SIZE = 20000;
	private static String topic = "topic_1";

	public static void main(String[] args) {
		
		Producer producer = new Producer();
		try {
			producer.start();
			producer.register();
			for (int i = 0; i < SIZE; i++) {
				Message message = Message.newInstance();
				message.setTopic(topic);
				message.setCmd(MessageCmdEnum.NEW);
				message.setSource(MessageSourceEnum.PRODUCER);
				message.setTarget(MessageSourceEnum.BROKER);
				Result result = producer.send(message);
				System.out.println(result);
			}
		} finally {
			producer.end();
		}

	}
}
