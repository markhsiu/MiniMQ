package com.markhsiu.minimq.netty;

import com.markhsiu.minimq.broker.Broker;

/**
 * Created by Mark Hsiu on 2017/2/8.
 */
public class ServerTest {

    public static void main(String[] args) {
        Broker broker = new Broker();	
        broker.init("broker1");
        broker.start();
    }
}
