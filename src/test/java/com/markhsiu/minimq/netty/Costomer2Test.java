package com.markhsiu.minimq.netty;

import com.markhsiu.minimq.client.CallBackor;
import com.markhsiu.minimq.client.consumer.Consumer;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.Result;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;

/**
 * Created by Mark Hsiu on 2017/2/8.
 */
public class Costomer2Test {
	
	static String topic = "topic_2";
	static String consumerID = "consumer_2";
 
	public static void main(String[] args) {
 
		Consumer customer = new Consumer(consumerID, new CallBackor() {	
			@Override
			public Message print(Result result) {
				System.out.println(result);
				Message response = Message.newInstance();
	        	response.setConsumerID(consumerID);
	        	response.setSource(MessageSourceEnum.CUSTOMER);
				response.setTarget(MessageSourceEnum.BROKER);
	        	response.setCmd(MessageCmdEnum.SUCCESS);
	        	return response;
			}
		});
		
		try {
			customer.start();
			customer.register();
			customer.subscribe(topic);
		} catch(Exception e) {
			customer.end();
			e.printStackTrace();
		}

	}
}
