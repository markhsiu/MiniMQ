package com.markhsiu.minimq.zookeeper;

import java.io.IOException;
import java.util.List;

import org.apache.zookeeper.KeeperException;

public class ClientTest {

	
	public ClientTest(){
	}

    

    public static void main(String[] args) throws IOException, InterruptedException, KeeperException {
    	 ZkClient zk = ZkUtils.getZkClient();
    	 List<String> list = zk.getChildren(ZkUtils.ROOT_BROKER);
    	 System.out.println(list);
    	 for (String broker : list) {
			System.out.println(broker);
		}
    }
}
