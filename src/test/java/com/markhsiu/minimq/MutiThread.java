package com.markhsiu.minimq;

import java.util.concurrent.CountDownLatch;

import com.markhsiu.minimq.client.producer.Producer;
import com.markhsiu.minimq.message.Message;
import com.markhsiu.minimq.message.constant.MessageCmdEnum;
import com.markhsiu.minimq.message.constant.MessageSourceEnum;

public class MutiThread implements Runnable {

	private CountDownLatch signal;
	private CountDownLatch finish;
	private Producer producer;

	public MutiThread(Producer producer, CountDownLatch signal, CountDownLatch finish) {
		this.producer = producer;
		this.signal = signal;
		this.finish = finish;
	}

	public void run() {

		try {
			signal.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		Message message = Message.newInstance();
		message.setTopic("topic");
		message.setCmd(MessageCmdEnum.NEW);
		message.setSource(MessageSourceEnum.PRODUCER);
		message.setTarget(MessageSourceEnum.BROKER);
		producer.send(message);
		 
	    finish.countDown();
	}

}
