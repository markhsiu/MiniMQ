package com.markhsiu.minimq.store;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import com.markhsiu.minimq.broker.store.disk.TopicDiskPool;
import com.markhsiu.minimq.broker.store.disk.TopicDiskQueue;
import com.markhsiu.minimq.message.Message;

public class DiskWriteTest {

	public static void main(String[] args) throws IOException {
		
		TopicDiskPool.builder();
		long start = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			String topic = "topic"+i;
			
			for (int j = 0; j < 1000000; j++) {
				Message message = new Message();
				message.setTopic(topic);
				message.setMessageID(i+"-"+j);
				TopicDiskPool.putMsg(message);
			}
		}
		System.out.println(( System.currentTimeMillis() - start) +" ms");
		Map<String, TopicDiskQueue> queues = TopicDiskPool.queues();
		for (Entry<String, TopicDiskQueue> queue : queues.entrySet()) {
			System.out.println(queue.getValue().toString());
		}
		System.out.println(( System.currentTimeMillis() - start) +" ms");
		TopicDiskPool.destory();
		
		System.out.println(( System.currentTimeMillis() - start) +" ms");
	}
}
