package com.markhsiu.minimq.store;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import com.markhsiu.minimq.broker.store.disk.TopicDiskPool;
import com.markhsiu.minimq.broker.store.disk.TopicDiskQueue;

public class DiskReadTest {

	public static void main(String[] args) throws IOException {
		
		TopicDiskPool.builder();
		long start = System.currentTimeMillis();
		for (int i = 0; i < 10; i++) {
			String topic = "topic"+i;
			while(true){
				if(TopicDiskPool.pollMsg(topic) == null){
					break;
				}
			}
			
		}
		System.out.println(( System.currentTimeMillis() - start) +" ms");
		Map<String, TopicDiskQueue> queues = TopicDiskPool.queues();
		for (Entry<String, TopicDiskQueue> queue : queues.entrySet()) {
			System.out.println(queue.getValue().toString());
		}
		System.out.println(( System.currentTimeMillis() - start) +" ms");
		TopicDiskPool.destory();
		
		System.out.println(( System.currentTimeMillis() - start) +" ms");
	}
}
