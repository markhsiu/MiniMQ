#MiniMQ
目前项目是起步阶段，是自己学习netty的延伸出来。因为写这个项目，又发现了一个有趣好玩的通信框架[talent-aio](https://git.oschina.net/tywo45/talent-aio)，这个在真的很快，用起来很爽，源码很值得看，也许将来完善后，可以替代netty，最重要是中国人写的，必须支持，up up up。近期看了阿里新出来的
《[阿里巴巴Java开发手册](doc/阿里巴巴Java开发手册.pdf)》，感觉蛮好的
，可以作为项目的编码标准。 
设计原则参考dubbo作者梁飞的一篇分享《[架构设计原则](doc/架构设计原则.pdf)》。
* 第一阶段主要学习一些别人的轮子MQ，看看简单的MQ设计思路，多方借鉴好的结构 
 ```
      >>> https://github.com/tang-jie/AvatarMQ.git    
          	作者有 [博客园](http://www.cnblogs.com/jietang/) 的专栏专门讲解 ，是自己学习其他MQ产品练习出来的项目。代码不单单是来看的，也是来写的。
      >>> https://github.com/uncodecn/uncode-mq.git    
          	作者oschina [blog](https://my.oschina.net/uncode)，设计还算完整，但是性能部分很多没有考虑，比如序列化部分还是使用jdk的io。  
      >>> https://git.oschina.net/xuxueli0323/xxl-mq   
         	作者 [blog](http://www.cnblogs.com/xuxueli/p/4918535.html)   
      >>> https://git.oschina.net/blackbeans/kiteq
         	go语言版本 
      >>> https://git.oschina.net/itteam/sf-mq
         	被坑，难怪看着设计这么复杂，原来是阿里MQ的3.x版本 
      >>> https://git.oschina.net/rushmore/zbus    
         	比较活跃，也是比较完整可靠的非主流MQ实现 
      >>> https://github.com/adyliu/jafka  
         	Kafka的克隆版本,由原来的scala换成java实现 
      >>> https://github.com/fusesource/mqtt-client  
         	mqtt,作为以为高效客户端连接的参考 
      >>> https://github.com/killme2008/Metamorphosis/
      		rocketmq的早期版本，Meta.任何好的框架都是由简入繁，学习早期的设计，反而更有利于理解它的进化
```

* 第二阶段阅读和参看主流java类的MQ框架，比如RocketMQ，ActiveMQ.以阿里的RocketMQ为主,也会参看
Kafka(scala),RabbitMQ(Erlang),ZeroMQ(C/C++) 
 ```
    >>> https://github.com/apache/incubator-rocketmq 
    >>> https://github.com/apache/activemq
```

* 第三阶会重写这个项目部分依赖的框架，作为下一步的学习

#模块划分
- Broker 简单来说就是消息队列服务器实体。
   	* factory 消费者管理等
   	* server 提供客户端连接
   	* task 消息推送
   	* store 消息存储管理  
   	* topic：路由关键字 Routing Key，
- core 工具类，和常量配置   	
- Message 消息实体，客户端和服务端传递信息的载体
- client 连接服务端 
   	* Producer：消息的生产者,主要用来发送消息给消费者。  
    * Consumer：消息的消费者， 主要用来接收生产者的消息。
   主要用来控制生产者和消费者之间的发送与接收消息的对应关系。
- Channel：消息通道
 在客户端的每个连接里，可建立多个channel，每个channel代表一个会话任务。
- Serialize 序列化接口和实现 

#zookeeper数据结构
* broker启动后注册到zookeeper，通知cosumer、producer更新broker信息
* cosumer启动后,获取所有的broker信息，尝试连接包含topic的broker
* producer启动， 获取所有的broker信息，
* broker等待producer发送消息，注册接受的topic到对应的brokerID下,并通知cosumer更新broker信息

```
Borker
/minimq/broker/${brokerID}(ip:port)/${topic}

```

#开发工具
* eclipse 4.5+,IDEA也可以
* maven3.x
* git
* JDK1.8+ 
 
#编码原则
* 尽量用单例，类不要有可以存储数据的属性（数据传输对象除外）
* 建议使用ConcurrentHashMap 来数据内存数据，并估算容器大小
* 面向接口编程，序列化、通信等模块通过注入实现接口调用  
* 日志输出不能使用System.out ,使用log4j并严格控制日志数据级别，最好使用xml配置异步处理，性能影响太大

#任务说明
* 默认为protostuff 序列化方式，支持java序列化
* netty作为默认通信方式 ，扩展[talent-aio](https://git.oschina.net/tywo45/talent-aio)通信方式
* 参考dubbo,用spring作为bean管理和功能扩展
* 存储方案 msyql 或是  redis 或是 磁盘
* 集群管理 zookeeper 
 
 